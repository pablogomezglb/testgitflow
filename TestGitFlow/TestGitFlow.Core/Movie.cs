﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestGitFlow.Core
{
    public class Movie
    {
        public int Id { get; set; }

        [Required, StringLength(150)]
        public string Name { get; set; }

        [Required, StringLength(10)]
        public string Rating { get; set; }

        public Genre Genre { get; set; }

        public string PosterUrl { get; set; }
    }
}
