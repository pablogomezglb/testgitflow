﻿using System;
using System.Web;

namespace TestGitFlow.Core
{
    public enum Genre 
    {
        Action,
        Adventure,
        Biography,
        Comedy,
        Crime,
        Drama,
        Fantasy,
        Horror,
        Music,
        SciFi
    }
}
