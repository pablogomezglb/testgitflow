﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TestGitFlow.Core;

namespace TestGitFlow.Data
{
    public class InMemoryMovieData : IMovieData
    {
        readonly List<Movie> movies;

        public InMemoryMovieData()
        {
            movies = new List<Movie>()
            {
                new Movie { Genre = Genre.Action, Id = 1, Name = "John Wick", PosterUrl = "/images/JohnWick.jpg", Rating = "R" },
                new Movie { Genre = Genre.Action, Id = 2, Name = "Black Widow", PosterUrl = "/images/BlackWidow.jpg", Rating = "PG-13" },
                new Movie { Genre = Genre.Horror, Id = 3, Name = "The Silence of the Lambs", PosterUrl = "/images/TheSilenceOfTheLambs.jpg", Rating = "R" },
                new Movie { Genre = Genre.Comedy, Id = 4, Name = "Back to the Future", PosterUrl = "/images/BackToTheFuture.jpg", Rating = "PG" },
                new Movie { Genre = Genre.SciFi, Id = 5, Name = "Men in Black", PosterUrl = "/images/MenInBlack.jpg", Rating = "PG-13" },
            };
        }

        public Movie Add(Movie movie)
        {
            movie.Id = movies.Max(m => m.Id) + 1;

            movies.Add(movie);
            movie.PosterUrl = CheckImageFile(movie.PosterUrl);

            return movie;
        }

        public int Commit()
        {
            return 0;
        }

        public Movie GetMovieById(int id)
        {
            return movies.FirstOrDefault(m => m.Id == id);
        }

        public IEnumerable<Movie> GetMovies()
        {
            return movies.ToList().OrderBy(m => m.Name);
        }

        public int GetMoviesCount()
        {
            return movies.Count;
        }

        public Movie Update(Movie movie)
        {
            Movie currentMovie = new Movie();

            if (movies.Any(m => m.Id == movie.Id))
            {
                currentMovie = movies.FirstOrDefault(m => m.Id == movie.Id);

                currentMovie.Genre = movie.Genre;
                currentMovie.Name = movie.Name;
                currentMovie.Rating = movie.Rating;
                currentMovie.PosterUrl = CheckImageFile(movie.PosterUrl);
            }

            return currentMovie;
        }

        private string CheckImageFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                return "/images/DefaultImage.jpg";
            }
            else
            {
                return File.Exists(string.Format(@"{0}\wwwroot\{1}",Directory.GetCurrentDirectory(), filePath)) ? filePath : "/images/DefaultImage.jpg";
            }
        }
    }
}
