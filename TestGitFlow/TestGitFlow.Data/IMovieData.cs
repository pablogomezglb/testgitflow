﻿using System;
using System.Collections.Generic;
using System.Text;
using TestGitFlow.Core;

namespace TestGitFlow.Data
{
    public interface IMovieData
    {
        Movie Add(Movie movie);

        int Commit();

        IEnumerable<Movie> GetMovies();

        Movie GetMovieById(int id);

        int GetMoviesCount();

        Movie Update(Movie movie);
    }
}
