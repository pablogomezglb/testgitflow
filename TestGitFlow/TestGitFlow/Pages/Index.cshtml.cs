﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestGitFlow.Core;
using TestGitFlow.Data;

namespace TestGitFlow.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IConfiguration _config;
        private readonly ILogger<IndexModel> _logger;
        private readonly IMovieData _movieData;

        public IEnumerable<Movie> movies { get; private set; }

        public IndexModel(IConfiguration config, ILogger<IndexModel> logger, IMovieData movieData)
        {
            _config = config;
            _logger = logger;
            _movieData = movieData;
        }

        public void OnGet()
        {
            movies = _movieData.GetMovies();
        }
    }
}
