using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TestGitFlow.Core;
using TestGitFlow.Data;

namespace TestGitFlow.Pages
{
    public class DetailModel : PageModel
    {
        private readonly IMovieData movieData;

        public Movie Movie { get; set; }

        [TempData]
        public string Message { get; set; }

        public DetailModel(IMovieData movieData)
        {
            this.movieData = movieData;
        }

        public IActionResult OnGet(int movieId)
        {
            if (movieData.GetMovies().Any(m => m.Id == movieId))
            {
                Movie = movieData.GetMovieById(movieId);
            }
            else
            {
                RedirectToPage("./NotFound");
            }

            return Page();

        }
    }
}
