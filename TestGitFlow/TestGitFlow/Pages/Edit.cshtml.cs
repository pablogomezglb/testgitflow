using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using TestGitFlow.Core;
using TestGitFlow.Data;

namespace TestGitFlow.Pages
{
    public class EditModel : PageModel
    {
        private readonly IMovieData movieData;
        private readonly IHtmlHelper htmlHelper;

        [BindProperty]
        public Movie Movie { get; set; }

        public IEnumerable<SelectListItem> Genres  { get; set; }

        public EditModel(IMovieData movieData, IHtmlHelper htmlHelper)
        {
            this.movieData = movieData;
            this.htmlHelper = htmlHelper;
        }

        public IActionResult OnGet(int? movieId)
        {
            Genres = htmlHelper.GetEnumSelectList<Genre>();

            if (movieId.HasValue)
            {
                if (movieData.GetMovies().Any(m => m.Id == movieId))
                {
                    Movie = movieData.GetMovieById(movieId.Value);
                }
                else
                {
                    return RedirectToPage("./NotFound");
                }
            }
            else
            {
                Movie = new Movie();
            }

            return Page();
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Movie = Movie.Id > 0 ? movieData.Update(Movie) : movieData.Add(Movie);
            movieData.Commit();

            TempData["Message"] = "Movie saved !";

            return RedirectToPage("./Detail", new { movieId = Movie.Id });
        }
    }
}
